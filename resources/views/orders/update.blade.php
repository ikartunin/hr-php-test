@extends('app')
@section('content')
    <h1>Заказ №{{ $Order->id }}</h1>
    <div class="content">
        {{ Form::open(['route' => ['orders.update', $Order->id]]) }}
            <input type="hidden" name="_method" value="put" />
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" name="client_email" value="{{ $Order->client_email }}" id="email">
            </div>
            <div class="form-group">
                <label for="partner">Партнёр</label>
                <select class="form-control" name="partner_id" id="partner">
                    @foreach($Partners as $partner)
                        <option @if($partner->id == $Order->partner_id) selected @endif value="{{ $partner->id }}">{{ $partner->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Продукты</label>
                <ul>
                    @foreach($Order->orderProducts as $orderProduct)
                        <li><span class="col-md-6">{{ $orderProduct->product->name }}</span><span>{{ $orderProduct->quantity }} шт.</span></li>
                    @endforeach
                </ul>
            </div>
            <div class="form-group">
                <label for="status">Статус</label>
                <select class="form-control" name="status" id="status">
                    <option value="0">новый</option>
                    <option value="10" @if($Order->status == 10) selected @endif >подтвержден</option>
                    <option value="20" @if($Order->status == 10) selected @endif >завершен</option>
                </select>
            </div>
            <div class="form-group">
                <label>Стоимость заказа</label>
                {{ $Order->orderPrice() }}
            </div>


            <input type="submit" class="btn btn-primary" value="Обновить">
        {{ Form::close() }}
    </div>
@stop
