@extends('app')
@section('content')
    <h1>Список заказов</h1>

    <div class="content">
        <table class="table">
            <tr>
                <th>ид заказа</th>
                <th>название партнера</th>
                <th>стоимость заказа</th>
                <th>наименование состав заказа</th>
                <th>статус заказа</th>
            </tr>
        @foreach($Orders as $order)
            <tr>
                <td><a href="{{ route('orders.edit', $order->id) }}">{{ $order->id }}</td>
                <td>{{ $order->partner->name }}</td>
                <td>{{ $order->orderPrice() }}</td>
                <td>
                    <ul>
                        @foreach($order->products as $product)
                            <li>{{ $product->name }}</li>
                        @endforeach
                    </ul>
                </td>
                <td>{{ $order->status() }}</td>
            </tr>
        @endforeach
        </table>
    </div>
@stop
