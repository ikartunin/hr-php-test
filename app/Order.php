<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    // Перебираем продукты из заказа, что бы сосчитать сумму заказа
    public function orderPrice()
    {
        $totalPrice = 0;
        foreach ($this->orderProducts as $product) {
            $totalPrice+= $product->price * $product->quantity;
        }
        return $totalPrice;
    }
    // Преобразуем id статуса заказа в текст
    public function status()
    {
        $status = [0 => 'новый', 10 => 'подтвержден', 20 => 'завершен'];
        return $status[$this->status];
    }
    // Методы для определения отношений между моделями
    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class);
    }
    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_products');
    }
    public function partner()
    {
        return $this->hasOne(Partner::class, 'id', 'partner_id');
    }
}
