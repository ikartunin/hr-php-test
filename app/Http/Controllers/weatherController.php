<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class weatherController extends Controller
{
    function __invoke()
    {
        $temp = '';
        // Получаем данные от яндекса
        $client = new \GuzzleHttp\Client(['headers' => ['X-Yandex-API-Key' =>'b93f828d-046d-4c6b-87f5-5710425aa4e0']]);
        $res = $client->request('GET', 'https://api.weather.yandex.ru/v1/informers?lon=34.374304&lat=53.279268');
        if ($res->getStatusCode() == 200)
            $temp = json_decode($res->getBody())->fact->temp;
        // Если температура выше 0 - добавляем знак "+"
        if (intval($temp) > 0)
            $temp = '+'.$temp;
        // Выводим результат
        return view('weather', ['temp' => $temp]);
    }
}
