<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Выводим все заказы
        $Orders = \App\Order::orderBy('id', 'desc')->limit(20)->get();
        return view('orders.index', ['Orders' => $Orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Order = \App\Order::find($id);
        return view('orders.update', ['Order' => $Order, 'Partners' => \App\Partner::orderBy('name')->get()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Проверяем указан ли email, и формат email
        $request->validate([
            'client_email' => 'required|email',
        ]);
        // Берём модель
        $Order = \App\Order::find($id);
        // Обновляем параметры
        $Order->client_email = $request->client_email;
        $Order->partner_id   = $request->partner_id;
        $Order->status       = $request->status;
        // Сохраняем
        $Order->save();
        // Возвращаемся на страницу заказа
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
