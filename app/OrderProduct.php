<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    // Методы для определения отношений между моделями
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}
